level_0 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (110, 400),
    'unlock': 1,
    'node_graphics': 'Level/graphics/overworld/0',
}

level_1 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (320, 220),
    'unlock': 2,
    'node_graphics': 'Level/graphics/overworld/1',
}

level_2 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (480, 610),
    'unlock': 3,
    'node_graphics': 'Level/graphics/overworld/2',
}

level_3 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (610, 350),
    'unlock': 4,
    'node_graphics': 'Level/graphics/overworld/3',
}

level_4 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (880, 210),
    'unlock': 5,
    'node_graphics': 'Level/graphics/overworld/4',
}

level_5 = {
    'coins': 'levels/0/level_0_coins.csv',
    'constraints': 'levels/0/level_0_constraints.csv',
    'crates': 'levels/0/level_0_crates.csv',
    'enemies': 'levels/0/level_0_enemies.csv',
    'palms': 'levels/0/level_0_fg_palms.csv',
    'grass': 'levels/0/level_0_grass.csv',
    'player': 'levels/0/level_0_player.csv',
    'terrain': 'levels/0/level_0_terrain.csv',
    'greens': 'levels/0/level_0_greens.csv',
    'node_pos': (1050, 400),
    'unlock': 5,
    'node_graphics': 'Level/graphics/overworld/5',
}

levels = {
    0: level_0,
    1: level_1,
    2: level_2,
    3: level_3,
    4: level_4,
    5: level_5,
}


