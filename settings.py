level_map = [
    '                              ',
    ' P                      XXXXX ',
    'XXXXXX          XXXXX    XXX  ',
    '        XXXXXX            X   ',
    '  XXX                         ',
    ' XX          XXXX             ',
    'XXX    XXX           XXXX    X',
    '       X      XXX          XXX',
    '    XXXX    XXX    XXX   XXX  ',
    'XXXXXXXX  XXXXX  XX  XXXXX    ',
]

tile_size = 64
screen_width = 1200
screen_height = len(level_map) * tile_size
