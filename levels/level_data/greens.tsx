<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.2" name="greens" tilewidth="313" tileheight="260" tilecount="11" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="70" height="45" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Cactus (2).png"/>
 </tile>
 <tile id="1">
  <image width="108" height="111" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Cactus (1).png"/>
 </tile>
 <tile id="2">
  <image width="145" height="88" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Bush (1).png"/>
 </tile>
 <tile id="3">
  <image width="102" height="50" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Grass (1).png"/>
 </tile>
 <tile id="4">
  <image width="102" height="50" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Grass (2).png"/>
 </tile>
 <tile id="5">
  <image width="124" height="73" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Stone.png"/>
 </tile>
 <tile id="6">
  <image width="101" height="99" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/StoneBlock.png"/>
 </tile>
 <tile id="7">
  <image width="313" height="260" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Tree.png"/>
 </tile>
 <tile id="8">
  <image width="150" height="51" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Skeleton.png"/>
 </tile>
 <tile id="9">
  <image width="84" height="87" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/SignArrow.png"/>
 </tile>
 <tile id="10">
  <image width="85" height="88" source="../../../../Downloads/Adventure-Girl-master/Adventure-Girl-master/Assets/Sprites/World/Objects/Sign.png"/>
 </tile>
</tileset>
